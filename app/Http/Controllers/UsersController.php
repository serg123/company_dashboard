<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
        $data=User::paginate(5);
        return view('users.index',["data"=>$data]);
    }
}
