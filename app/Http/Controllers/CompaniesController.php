<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use Storage;

class CompaniesController extends Controller
{
    public function index()
    {
        $data = Company::paginate(5);
        return view('companies.index', ["data" => $data]);
    }

    public function create()
    {
        return view('companies.create');
    }

    public function edit($id)
    {
        $company = Company::find($id);
        return view('companies.edit', ['data' => $company]);
    }

    public function store(Request $request)
    {
        $this->control($request);
        $company = new Company;
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->website = $request->input('website');
        $company->logo = $this->storeLogo($request);
        $company->save();
        return redirect('companies');
    }

    public function update(Request $request, $id)
    {
        $this->control($request);
        $company = Company::find($id);
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->website = $request->input('website');
        $company->logo = $this->storeLogo($request);
        $company->save();
        return redirect('companies');
    }

    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        return redirect('companies');
    }

    private function storeLogo($request)
    {
        if ($request->hasFile("logo")) {
            $fileName = $request->logo->getClientOriginalName();
            $request->logo->storeAs('logos', $fileName, 'public');
            return $fileName;
        } else return null;
    }

    private function control($request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'email' => 'email|max:50',
            'website' => 'max:50',
            'logo' => 'file|dimensions:min_width=150,min_height=150',
        ]);
    }
}
