<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;

class EmployeesController extends Controller
{
    public function index()
    {
        $data=Employee::paginate(5);
        return view('employees.index',["data"=>$data]);
    }

    public function create()
    {
        $companies=Company::all("id","name");
        return view('employees.create', ["companies"=>$companies]);
    }

    public function edit($id)
    {
        $employee=Employee::find($id);
        $companies=Company::all("id","name");
        return view('employees.edit',['data'=>$employee, "companies"=>$companies]);
    }

    public function store(Request $request)
    {
        $this->control($request);
        $employee=new Employee;
        $employee->first_name=$request->input('first_name');
        $employee->last_name=$request->input('last_name');
        $employee->email=$request->input('email');
        $employee->phone=$request->input('phone');
        $employee->company_id=$request->input('company');
        $employee->save();
        return redirect('employees');
    }

    public function update(Request $request, $id)
    {
        $this->control($request);
        $employee=Employee::find($id);
        $employee->first_name=$request->input('first_name');
        $employee->last_name=$request->input('last_name');
        $employee->email=$request->input('email');
        $employee->phone=$request->input('phone');
        $employee->company_id=$request->input('company');
        $employee->save();
        return redirect('employees');
    }

    public function destroy($id)
    {
        $employee=Employee::find($id);
        $employee->delete();
        return redirect('employees');
    }

    private function control($request)
    {
        $request->validate([
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'email|max:50',
            'phone' => 'regex:/[0-9]+/|max:50',
            'company' => 'integer|min:1',
        ]);
    }
}
