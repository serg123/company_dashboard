CREATE DATABASE company_dashboard;
CREATE USER 'company_dashboard_user'@'localhost' IDENTIFIED BY 'company_dashboard_password';
GRANT ALL ON company_dashboard.* TO 'company_dashboard_user'@'localhost';
