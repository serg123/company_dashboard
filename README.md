### Dashboard to manage companies
- Basic Laravel Auth: ability to log in as administrator and register.
- Use database seeds to create first user with email “admin@blueglass.ee” and password “7654321”.
- CRUD functionality (Create / Read / Update / Delete) for two menu items: Companies and Employees.
- Companies DB table consists of these fields: Name (required), email, logo (minimum 150×150), website.
- Employees DB table consists of these fields: First name (required), Last name (required), Company (foreign key to Companies), email, phone.
- Users DB table consists of these additional fields: company_id, avatar.
- Use database migrations to create those schemas above.
- Store companies logos in storage/app/public folder and make them accessible from public.
- On registration form, allow users to select Company from the select box.
- Use basic Laravel resource controllers with default methods – index, create, store etc.
- Use Laravel’s validation function, using Request classes.
- Use Laravel’s pagination for showing Companies/Employees list, 5 entries per page.
- Use Laravel’s pagination for showing Users list, 5 entries per page.
- Use Laravel make:auth as default Bootstrap-based design theme, but remove ability to register.

### Instructions
- run: composer install
- run db.sql to crete database and user
- set public folder as a web server root folder
- make link public/storage->storage/app/public (php artisan storage:link)
