<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::group(['middleware' => ['auth', 'web']], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('/companies', 'CompaniesController')->names(['create' => 'companies_create', 'index' => 'companies_index',
        'edit' => 'companies_edit', 'store' => 'companies_store', 'update' => 'companies_update', 'destroy' => 'companies_destroy']);
    Route::resource('/employees', 'EmployeesController')->names(['create' => 'employees_create', 'index' => 'employees_index',
        'edit' => 'employees_edit', 'store' => 'employees_store', 'update' => 'employees_update', 'destroy' => 'employees_destroy']);
    Route::resource('/users', 'UsersController')->names(['index' => 'users_index']);
});
