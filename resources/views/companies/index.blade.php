@extends('layouts.app')

@section('content')
<script>
    function destroy(action){
        var form=document.getElementById('destroy-form');
        form.action=action;
        form.submit();
    }
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Companies</div>
                <div class="card-body">
                    <button class="btn btn-success" onclick="window.location.href='{!!route('companies_create')!!}'">Create new company</button>
                    <table class="table mt-4">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Email</th>
                            <th scope="col">Website</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>{{ $d->name }}</td>
                                <td><img src="{{ url('storage/logos/'.$d->logo) }}" style="width:70px; height:70px"></td>
                                <td>{{ $d->email }}</td>
                                <td>{{ $d->website }}</td>
                                <td>
                                    <a class="btn btn-link text-primary" href="{{route('companies_edit', ['id' => $d->id]) }}">Edit</a>
                                    <a class="btn btn-link text-danger" href="javascript:destroy('{!!route('companies_destroy', ['id' => $d->id])!!}')">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->render() }}
                    <form id="destroy-form" action="" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
