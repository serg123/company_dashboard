@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create new employee</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('employees_store') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="fname" class="col-md-4 col-form-label text-md-right">First name</label>
                            <div class="col-md-6">
                                <input id="fname" type="text" name="first_name" class="form-control" required>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('first_name') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lname" class="col-md-4 col-form-label text-md-right">Last name</label>
                            <div class="col-md-6">
                                <input id="lname" type="text" name="last_name" class="form-control" required>
                                @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('last_name') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="email" name="email" class="form-control" required>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Phone</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" name="phone" class="form-control" required>
                                @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('phone') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="company" class="col-md-4 col-form-label text-md-right">Company</label>
                            <div class="col-md-6">
                                <select id="company" class="form-control" name="company">
                                    @foreach($companies as $c)
                                    <option value="{{$c->id}}">
                                        {{ $c->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-4 offset-md-4">
                                <button type="button" class="btn btn-default" onclick="window.location.href='{!!route('employees_index')!!}'">Cancel</button>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
