@extends('layouts.app')

@section('content')
<script>
    function destroy(action){
        var form=document.getElementById('destroy-form');
        form.action=action;
        form.submit();
    }
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Employees</div>
                <div class="card-body">
                    <button class="btn btn-success" onclick="window.location.href='{!!route('employees_create')!!}'">Create new employee</button>
                    <table class="table mt-4">
                        <thead>
                        <tr>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Company</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>{{ $d->first_name }}</td>
                                <td>{{ $d->last_name }}</td>
                                <td>{{ $d->email }}</td>
                                <td>{{ $d->phone }}</td>
                                <td>{{ $d->company->name }}</td>
                                <td>
                                    <a class="btn btn-link text-primary" href="{{route('employees_edit', ['id' => $d->id]) }}">Edit</a>
                                    <a class="btn btn-link text-danger" href="javascript:destroy('{!!route('employees_destroy', ['id' => $d->id])!!}')">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->render() }}
                    <form id="destroy-form" action="" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
