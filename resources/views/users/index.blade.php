@extends('layouts.app')

@section('content')
<script>
    function destroy(action){
        var form=document.getElementById('destroy-form');
        form.action=action;
        form.submit();
    }
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">Users</div>
                <div class="card-body">
                    <table class="table mt-4">
                        <thead>
                        <tr>
                            <th scope="col">Email</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>{{ $d->email }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $data->render() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
